package com.example.evotec.primerapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadRecivirParametro extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recivir_parametro);
        texto = (TextView) findViewById(R.id.lblParametro);
        Bundle bundle = this.getIntent() .getExtras();
        texto.setText(bundle.getString("dato"));
    }
}
